FROM registry.gitlab.com/enarx/dev:latest

# Install packages (no kernel)
RUN apt update \
 && apt install -y firmware-linux-free \
 && apt install -y man-db manpages-dev \
 && apt install -y libopenblas-dev \
 && apt install -y openssh-server \
 && apt install -y gfortran \
 && apt install -y iproute2 \
 && apt install -y locales \
 && apt install -y lua5.4 \
 && apt install -y podman \
 && apt install -y mdadm \
 && apt install -y mosh \
 && apt install -y wget \
 && apt install -y zsh \
 && apt install -y tmux \
 && apt install -y fish \
 && apt install -y liblua5.4-dev \
 && apt install -y cmake \
 && apt install -y unzip \
 && apt install -y fuse3 \
 && rm -rf /var/lib/apt/lists/*

# Generate locale data
RUN sed -i 's|# en_US|en_US|' /etc/locale.gen \
 && locale-gen

# Configure systemd
RUN rm -f /etc/machine-id /var/lib/dbus/machine-id
COPY init /init

# Enable networking
COPY wait-online.override /etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
COPY netinfo.service /etc/systemd/system/
COPY wyrnet.service /etc/systemd/system/
COPY wyrcan /usr/local/bin/wyrcan
COPY hosts /etc/
RUN systemctl enable systemd-resolved systemd-networkd netinfo wyrnet

# Install the persistent storage service
COPY fstab /etc/fstab
COPY mkdata /usr/local/libexec/mkdata
COPY mkdata.service /etc/systemd/system/mkdata.service
RUN systemctl enable mkdata.service

# Install the hostname detection service
COPY hostnames.service /etc/systemd/system/hostnames.service
COPY hostnames /usr/local/libexec/hostnames
COPY hostnames.conf /etc/hostnames.conf
RUN systemctl enable hostnames.service

# Install the user creation service
COPY users.service /etc/systemd/system/users.service
COPY nopassword /etc/sudoers.d/nopassword
COPY users /usr/local/libexec/users
COPY sshkeys.conf /etc/sshkeys.conf
COPY groups.conf /etc/groups.conf
COPY users.conf /etc/users.conf
RUN systemctl enable users.service

# Install the bash service
RUN systemctl mask console-getty.service serial-getty@.service getty@.service getty.target
#COPY bash.service /etc/systemd/system/bash.service
#RUN systemctl enable bash.service

# Install the ssh key persistence service
COPY sshcfg.service /etc/systemd/system/sshcfg.service
RUN systemctl enable sshcfg.service
RUN rm -f /etc/ssh/ssh_host_*

# Install the daily reboot service
COPY reboot.timer /etc/systemd/system/reboot.timer
RUN systemctl enable reboot.timer

# Install the GHA runner service
COPY linger@.service /etc/systemd/system/linger@.service
COPY gha@.service /etc/systemd/system/gha@.service
COPY gha /usr/local/libexec/gha
RUN for i in `seq 0 9`; do systemctl enable gha@enarx$i.service; done

# Install various configuration items
COPY wyrcan.cmdline /boot/wyrcan.cmdline
COPY 99-kvm.rules /etc/udev/rules.d/
